import React, { useState }from 'react';
import './App.css';
import NavBar from './components/NavBar';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import Register from './pages/Register';
import UserContext from './UserContext';
import NotFound from './pages/NotFound';
import Login from './pages/Login';
import Home from './pages/Home';
import AddProduct from './pages/AddProduct';
import Products from './pages/Products';
import ProductCard from './components/ProductCard'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import UserDetails from './pages/UserDetails'

function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'), 
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  });

   const unsetUser = () =>{
    localStorage.clear();
    setUser({
      accessToken: null,
      isAdmin: null})
  }

  return (

    <UserContext.Provider value ={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar />
          <Container>
            <Switch>
              <Route exact path="/" component ={Home} />
              <Route exact path="/register" component ={Register} />
              <Route exact path="/login" component ={Login} />
              <Route exact path="/products" component ={Products} />
              <Route exact path="/cart" component ={Cart} />
              <Route exact path="/orders" component ={Orders} />
              <Route exact path="/products/:id" component ={ProductCard} />
              <Route exact path="/addproduct" component ={AddProduct} />
              <Route exact path="/profile" component ={UserDetails} />
              <Route component={NotFound} />
            </Switch>
          </Container>
      </Router>
    </UserContext.Provider>

  );
}

export default App;
