import React, {useState, useEffect, useContext} from 'react';
import {Row, Col, Card, Button, Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Featured from '../components/Featured';
import UserContext from '../UserContext'


export default function Home() {

	const { user } = useContext(UserContext)

	const [featuredProducts, setFeaturedProducts] = useState([]);

useEffect(()=>{
		fetch('https://arcane-depths-57641.herokuapp.com/products/active-products',{
			method: 'GET',
			headers:{
						Authorization: `BearproductId er ${user.accessToken}` 
					}
	
				}).then(res => res.json())
				  .then(data =>{
						console.log(data)

					setFeaturedProducts(data.map(product =>{
						if(product.name == "ASUS Monitor" || product.name == "Spectre Monitor" || product.name == "Samsung Monitor" || product.name == "test product")
							{
						return(
							<Col sm="3" lg="3" md="3" >
							<Featured key={product.id} product = {product} productId={product._id} />
							</Col>
							)
							}
					}))
			    		
							 
				 });


		}, []);



	return(
		<>
		<Banner />
		<Container fluid>
			<Row className="justify-content-center mt-3"><h2>Featured Products</h2></Row>
		<hr />
			<Row >
				{featuredProducts}
			</Row>
		</Container>
		</>
		

		)
}