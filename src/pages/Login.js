import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { Redirect, useHistory } from 'react-router-dom'

export default function Login(){

	const history = useHistory();

	const { user, setUser } = useContext(UserContext);
	

	const [email, setEmail] = useState('');

	const [password, setPassword] = useState('');

	

	const [loginButton, setLoginButton] = useState(false)

	

	useEffect(() =>{
		let unmounted = false;

		if((email !== '' && password !== '' && (!unmounted))){
			setLoginButton(true)
			
			return () => {
			unmounted = true;
			} 

		}else{
			setLoginButton(false)
		}
	}, [email, password])


	

	function loginUser(e){
		
		e.preventDefault(); 

		fetch('https://arcane-depths-57641.herokuapp.com/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
	})

		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken)
				setUser({ accessToken: data.accessToken });

				Swal.fire({
				
				title:"Yaaaay!!!",
				icon: "success",
				text: "Thank you for logging in to Stuff Shop"
				
				})

				

				fetch('https://arcane-depths-57641.herokuapp.com/users/details',{
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email);
						localStorage.setItem('isAdmin', data.isAdmin)

						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						history.push('/products')
					}else{
						history.push('/')
					}

				})

			}else{
				Swal.fire({
				
				title:"Oooops!!!",
				icon: "error",
				text: "Something Went Wrong. Check your Credentials"
				
				})
			}

			setEmail('')
			setPassword('')

		})

	}

	


	
	if(user.accessToken !== null){
		return <Redirect to="/" />
	}
	

	return(
	<Form onSubmit={(e)=> loginUser(e)}>
		<Form.Text>
				<h4>Login</h4>
		</Form.Text>
		<Form.Group controlId="userEMail">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}required/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>
		</Form.Group>

		<Form.Group controlId="password">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" onChange={e => setPassword(e.target.value)}value={password} required/>
		</Form.Group>

		{ loginButton ?
		<Button variant="success" type="submit">Submit</Button>
		:
		<Button variant="success" type="submit" disabled>Submit</Button>
		}

	</Form>
		)
}