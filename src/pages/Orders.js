import React, {useEffect, useState} from 'react';
import {Container, Button, Fade} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function Orders(product){

const [orders, setOrders] = useState([])
const [open, setOpen] = useState(false)





 useEffect(()=>{


 		 fetch('https://arcane-depths-57641.herokuapp.com/users/user-orders',{
 		 	method: 'GET',
 		 	headers:{
 		 		'Content-Type': 'application/json',
 		 		'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
 		 	}
 		 })
 		 .then(res => res.json())
 		 .then(data=>{
 		 	console.log(data)
 		 	setOrders(data.map(product=>{
 		 		return(
 		 			<ul key={product.id}>
 		 				<Link to={`/products/${product.productId}`}><li>{product.productId}</li></Link>
 		 				<p><strong> Total Price: </strong> &#8369; {product.totalAmount}</p>

 		 			</ul>
 		 			)
 		 	}))
 		 })


 }, [])

 



	return(
		<Container>
		<h2 className="text-center mt-3 mb-5">Order History</h2>
      <Button 
       	variant="success"
        onClick={() => setOpen(!open)}
        aria-controls="example-fade-text"
        aria-expanded={open}
      >
        Purchased Items({orders.length}) (Click for details)
      </Button>
      <Fade in={open}>
        <div id="example-fade-text"> <br />
          <p>Items:</p>
          	{orders}
        </div>
      </Fade>
    </Container>
		)
}