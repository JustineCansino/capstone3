import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { Redirect, useHistory } from 'react-router-dom'

export default function Register(){
	//let's define state hooks for all input fields

	const history = useHistory();


	const { user } = useContext(UserContext);

	const [email, setEmail] = useState('');

	const [mobileNo, setMobileNo] = useState('')

	const [password, setPassword] = useState('');


	const [verifyPassword, setVerifyPassword] = useState('');

	
	const [registerButton, setRegisterButton] = useState(false)

	

	useEffect(() =>{

		let unmounted = false

		if(( email !== '' && mobileNo != '' && password !== '' && verifyPassword !== '' ) && (mobileNo.length === 11) &&( password === verifyPassword ) && (!unmounted)){
			setRegisterButton(true)

			return () => {
			unmounted = true;
			}
			
		}else{
			
			setRegisterButton(false)
		}



	}, [email, mobileNo, password, verifyPassword]);


	function registerUser(e){

		e.preventDefault(); 

	

		fetch('https://arcane-depths-57641.herokuapp.com/users/register',{
			method: 'POST',
			headers:{'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password,
				mobileNo: mobileNo
				})
		})
		
		.then(res => res.text())
		.then(text =>{
			console.log(text)
			if(text !== 'true'){
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Registration failed. Please try again.'
				})
			}else{
			Swal.fire({
				
				title:"Yaaaaay!!!",
				icon: "success",
				text: "Congratulations! You have successfully registered"
				
				})
			}
			history.push('/login')
			


			setEmail('')
			setMobileNo('')
			setPassword('')
			setVerifyPassword('')
		})

		


	}

	if(user.accessToken !== null){
		return <Redirect to="/" />
	}
	

	return(
	<Form onSubmit={(e)=> registerUser(e)}>
		<Form.Text>
				<h4>Register</h4>
		</Form.Text>

		<Form.Group controlId="userEMail">
			<Form.Label>Email Address:</Form.Label>
			<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)}required/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>
		</Form.Group>

		<Form.Group controlId="mobile">
			<Form.Label>Mobile Number:</Form.Label>
			<Form.Control type="text" placeholder="Enter Mobile Number" onChange={e => setMobileNo(e.target.value)}value={mobileNo} required/>
		</Form.Group>

		<Form.Group controlId="password">
			<Form.Label>Password:</Form.Label>
			<Form.Control type="password" placeholder="Enter Password" onChange={e => setPassword(e.target.value)}value={password} required/>
		</Form.Group>

		<Form.Group controlId="password2">
			<Form.Label>Verify Password:</Form.Label>
			<Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required/>
		</Form.Group>

		{ registerButton ?
		<Button variant="primary" type="submit">Submit</Button>
		:
		<Button variant="primary" type="submit" disabled>Submit</Button>
		}

	</Form>
	
		)
}