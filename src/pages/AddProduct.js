import React, { useState, useEffect, useContext }from 'react'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Form, Button } from 'react-bootstrap';


export default function AddProduct(){

	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(true);

useEffect(() =>{

		if((name !== '' && description !== '' && price !== 0)){
			setIsActive(true)
			
		}else{
			setIsActive(false)
		}
	}, [name, description, price])

function addProduct(e){
		e.preventDefault();

		fetch('https://arcane-depths-57641.herokuapp.com/products/create-product',{
			method: 'POST',
			headers:{'Content-Type': 'application/json',
					Authorization: `Bearer ${user.accessToken}`
					},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
				})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data === true){
			Swal.fire({
				
				title:"Awesome!",
				icon: "success",
				text: "New product has been successfully created"
				
				})
			}else{
				Swal.fire({
				
				title:"Opps!",
				icon: "error",
				text: "Creating New Product Failed"
				
				})
			}
			
		})

		setName('')
		setDescription('')
		setPrice('')
}
    
return(
	<Form onSubmit={(e)=> addProduct(e)}>
		<Form.Text>
				<h4>Add Product</h4>
		</Form.Text>
		<Form.Group controlId="name">
			<Form.Label>Product Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter Product Name" onChange={e => setName(e.target.value)}value={name} required/>
		</Form.Group>
		<Form.Group controlId="description">
			<Form.Label>Description:</Form.Label>
			<Form.Control type="text" placeholder="Enter Description" onChange={e => setDescription(e.target.value)}value={description} required/>
		</Form.Group>

		<Form.Group controlId="price">
			<Form.Label>Price:</Form.Label>
			<Form.Control type="number" onChange={e => setPrice(e.target.value)}value={price} required/>
		</Form.Group>

		{ isActive ?
		<Button variant="success" type="submit">Submit</Button>
		:
		<Button variant="dark" type="submit" disabled>Submit</Button>
		}

	</Form>
	
		)
}