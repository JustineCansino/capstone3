import React, {useState, useEffect} from 'react';
import {Container, Form} from 'react-bootstrap'

export default function UserDetails(){


const [email, setEmail] = useState("")
const [mobileNo, setMobileNo] = useState("")




useEffect(()=>{

	fetch('https://arcane-depths-57641.herokuapp.com/users/details',{
		method: 'GET',
		headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
				}
		})
		.then(res => res.json())
		.then(data => {
		console.log(data)

		setEmail(data.email)
		setMobileNo(data.mobileNo)

		})

}, [])


return(
		<>
		<Form>
		<Form.Group>
		<Form.Label>Email address:</Form.Label>
		<Form.Control value={email} />
		</Form.Group>
		<Form.Group>
		<Form.Label>Mobile number:</Form.Label>
		<Form.Control value={mobileNo} />
		</Form.Group>
		</Form>
		</>
		)
	
}
