import React, {useState} from 'react';
import { Button, Table, Container, Card, Col, Row, FormControl} from 'react-bootstrap'
import ProductCard from '../components/ProductCard'
import InputGroup from 'react-bootstrap/InputGroup';
import Swal from 'sweetalert2'
import {Link} from 'react-router-dom'

export default function Cart(){

const productDetails = JSON.parse(localStorage.getItem('items'))

console.log(productDetails)

const [isOpen, setIsOpen] = useState(true)


function checkout(){


	fetch('https://arcane-depths-57641.herokuapp.com/users/checkout',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
		},
		body: JSON.stringify({
			productId: productDetails.id,
			price: productDetails.price,
			quantity: productDetails.quantity

		})
	})
	.then(res => res.json())
	.then(data=>{
		console.log(data)

		if(data === true){
			Swal.fire({
				title: 'Success',
				icon: 'success',
				text: 'Congratulation! Your order has been placed!  '
			})
		}else{
			Swal.fire({
				title: 'Error',
				icon: 'error',
				text: 'Something went wrong!'
			})
		}

	})
}

	if(productDetails == null){
		return(
			<h2 className="mt-3 text-center">Your cart is Empty. <Link to="/products"><span>Click here to Shop</span></Link></h2>
			)
	}else{

	return(
		<Container className="cart-bg">
			<h2 className="text-center mb-5 pt-2">Shopping Cart</h2>
		<Table striped bordered >
			<thead>
				<tr>
					<td><strong>Name</strong></td>
					<td><strong>Price</strong></td>
					<td><strong>Quantity</strong></td>
				</tr>
			</thead>

				<tr>
					<td>{productDetails.name}</td>
					<td>{productDetails.price}</td>
					<td>{productDetails.quantity}</td>
				</tr>
			</Table>
			<span><strong>Total Price:</strong> &#8369; {productDetails.price}  </span> <br /> <br />
			<Button className="cart-button"variant="success" onClick={()=>checkout()}>Checkout</Button>
		
		</Container>

		)
		}
}

