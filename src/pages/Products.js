import React, { useEffect, useContext, useState } from 'react';
import Product from '../components/Product'; 
import UserContext from '../UserContext'
import { Table, Container, Row, Col } from 'react-bootstrap'
import DeleteButton from '../components/DeleteButton'
import UpdateButton from '../components/UpdateButton'



export default function Products(){


	const [allProducts, setAllProducts] = useState([]);
	const [adminProducts, setAdminProducts] = useState([]);


	const { user } = useContext(UserContext)
		console.log(user);



	useEffect(()=>{
		fetch('https://arcane-depths-57641.herokuapp.com/products/active-products',{
			method: 'GET',
			headers:{
						Authorization: `Bearer ${user.accessToken}` 
					}
	
				}).then(res => res.json())
				  .then(data =>{
						console.log(data)
						
						setAdminProducts(data.map(product => {
							
							return(
								<tr key={product.id}>
									<td>{product._id}</td>
									<td>{product.name}</td>
									<td>{product.price}</td>
									<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
									<td><DeleteButton productId={product._id}/></td>
									<td><UpdateButton productId={product._id}/></td>
								</tr>
								  )

						}));
							setAllProducts(data.map(product =>{
								return(
									<Col sm="4" lg="4" className="mb-3 mt-3">
										<Product key={product.id} product = {product} productId={product._id} />
									</Col>
					 	  				)
			    			}));
	
							 
					});




		}, []);

		


	if(user.isAdmin !== true){
	return (
			<Container fluid>
			<Row className="justify-content-center mt-3"><h2>Our Products</h2></Row>
			<hr />
			<Row >
				{allProducts}
			</Row>
			</Container>	
		  );

	}else{
	return(
		<>
			<h1 className="text-center mb-5">Product Dashboard</h1>
			<Table striped bordered hover >
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Price</td>
						<td>Status</td>
					</tr>
				</thead>
				<tbody>
					{adminProducts}
					
				</tbody>
			</Table>
		</>
		);
	}

			
	
};

