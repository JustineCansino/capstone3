import React from 'react';
import { Link } from 'react-router-dom'
import { Container } from 'react-bootstrap'

export default function NoMatchPage () {
  return (
  		<Container>
    	 	<h3> 404 - Page Not found</h3>
    	 	<p> Go Back to <Link to="/">homepage</Link></p>
   		</Container>
	  	 )
	};