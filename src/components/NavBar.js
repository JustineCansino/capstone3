import React, { Fragment, useContext }from 'react';

import Navbar from 'react-bootstrap/Navbar'; 
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink, useHistory } from 'react-router-dom'
import userContext from '../UserContext'


export default function NavBar() {


	const history = useHistory();



	const { user, setUser, unsetUser } = useContext(userContext);
	console.log(user)

	const logout = () => {
		unsetUser();
		setUser({accessToken: null})
		history.push('/login')
	}


	let rightNav = ( user.accessToken !== null ) ?

			user.isAdmin
			?
			<Fragment>
				<Nav.Link className="text-light" as={NavLink} to="/addproduct">Add Product</Nav.Link>
				<Nav.Link className="text-light" as={NavLink} to="/orders">Orders</Nav.Link>
				<Nav.Link className="text-light" as={NavLink} to="/profile">Profile</Nav.Link>
				<Nav.Link className="text-light" onClick={logout}>Logout</Nav.Link>
			</Fragment>
			:
			<Fragment>
				<Nav.Link className="text-light" as={NavLink} to="/cart">Cart</Nav.Link>
				<Nav.Link className="text-light" as={NavLink} to="/orders">Orders</Nav.Link>
				<Nav.Link className="text-light" as={NavLink} to="/profile">Profile</Nav.Link>
				<Nav.Link className="text-light" onClick={logout}>Logout</Nav.Link>
			</Fragment>
		 :
		<Fragment>
			<Nav.Link className="text-light" as={NavLink} to="/login">Login</Nav.Link>
			<Nav.Link className="text-light" as={NavLink} to="/register">Register</Nav.Link>
		</Fragment>

	


	return(
	 <Navbar bg="dark" expand="lg">
	 	<Navbar.Brand  className="text-light" as={Link} to="/">TechWise</Navbar.Brand>
	 	<Navbar.Toggle aria-controls="basic-navbar-nav"/>
	 	<Navbar.Collapse id="basic-navbar-nav"> 

	 		<Nav className="mr-auto">
	 			<Nav.Link className="text-light" as={NavLink} to="/">Home</Nav.Link>
	 			<Nav.Link className="text-light" as={NavLink} to="/products">Products</Nav.Link>
	 		</Nav>
	 		<Nav className="ml-auto"> 
	 			{rightNav}
	 		</Nav>
	 	</Navbar.Collapse>
	 </Navbar>
		)

}