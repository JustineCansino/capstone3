import React, { useState } from 'react';
import {Row, Col, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { Link } from'react-router-dom'
import PropTypes from 'prop-types';



export default function Product({product, productId}) {


const { name, description, price} = product



	

	return(

			<Card className="cardProduct">
				<Card.Body>
					<Card.Title className="text-info">
						<h4>{name}</h4>
					</Card.Title>
					<Card.Text>
						<h6>Description:</h6> 
						<p>{description}</p>
					</Card.Text>
					<Card.Text>
						<p><strong>Price:</strong> PHP {price}</p> 
					</Card.Text>

					<Link to={`/products/${productId}`}>
					<Button variant="info" >View</Button>
					</Link>
						
				</Card.Body>
			</Card>

		)
}


Product.propTypes = {
	
	product: PropTypes.shape({
		
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}