import React from 'react';
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner() {
	return(

	<Row>
		<Col>
			<Jumbotron>
				<div className="text-center">
				<h1>TechWise</h1>
				<p><em>Technology and Gadgets for everyone!</em></p>
				<Link to="/products">
				<Button className="bg-primary" >Browse Products</Button>
				</Link>
				</div>
			</Jumbotron>
		</Col>
	</Row>




		)
}