import React, { useState, useEffect }from 'react';
import { Modal, Button, Form, } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom'

export default function UpdateButton({productId}){

	const history = useHistory()

	const [ show, setShow ] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	//forms
	const [ name, setName ] = useState('');
	const [ description, setDescription] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ isActive, setIsActive ] = useState(true);

	useEffect(()=>{
		if((name !== '' && description !== '' && price !== 0)){
			setIsActive(true)
			
		}else{
			setIsActive(false)
		}
	}, [name, description, price])

	function updateProduct(){
		fetch(`https://arcane-depths-57641.herokuapp.com/products/${productId}/update-product`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Product Created',
					icon: 'success',
					text: 'Product successfully updated'
				})
				.then(()=>history.go(0))
			}else{
				Swal.fire({
					title: 'Product Creation Failed',
					icon: 'error'
				})
			}

		})
	}

	return(
	    <>
			<Button variant="primary" onClick={handleShow}>Update</Button>

			<Modal show={show} onHide={handleClose}>
	        <Modal.Header closeButton>
	          <Modal.Title>Update a Product</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>
	      		<Form>
	      			<Form.Group>
	      				<Form.Label>Name:</Form.Label>
	      				<Form.Control
	      				type="text"
	      				placeholder="Enter Updated Product Name"
	      				value={name}
	      				onChange={(e)=>setName(e.target.value)}
	      				required 
	      				/>
	      			</Form.Group>

	      			<Form.Group>
	      				<Form.Label>Description:</Form.Label>
	      				<Form.Control
	      				type="text"
	      				placeholder="Enter Updated Description"
	      				value={description}
	      				onChange={(e)=>setDescription(e.target.value)}
	      				required 
	      				/>
	      			</Form.Group>

	      			<Form.Group>
	      				<Form.Label>Price:</Form.Label>
	      				<Form.Control
	      				type="number"
	      				value={price}
	      				onChange={(e)=>setPrice(e.target.value)}
	      				required 
	      				/>
	      			</Form.Group>



	      		</Form>  	





	        </Modal.Body>
	        <Modal.Footer>
	          <Button variant="secondary" onClick={handleClose}>
	            Close
	          </Button>
	      
	          { 
	          isActive ?
			  <Button variant="success" type="submit" onClick={updateProduct}>Save Changes</Button>
			  :
			  <Button variant="dark" type="submit" disabled>Save Changes</Button>
			  }

	        </Modal.Footer>
	      </Modal>
	  </>	
		)
}