import React, {useState, useEffect, useContext} from 'react';
import {useParams, Link} from 'react-router-dom';
import { Text, Row, Col, Card, Button, FormControl } from 'react-bootstrap';
import InputGroup from 'react-bootstrap/InputGroup';
import UserContext from '../UserContext'
import Cart from '../pages/Cart'



export default function ProductCard(){

	const { user } = useContext(UserContext)

	const { id } = useParams()

	const [cart, setCart] = useState([])
	
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [quantity, setQuantity] = useState(1)

	const [isOpen, setIsOpen] = useState(true)

	const [cartButton, toRegister] = useState(true)

	const incNum = () => {
	setQuantity(quantity + 1)
	setIsOpen(true)
	}



	const decNum = () => {
	if(quantity > 1){
			setQuantity(quantity - 1)
			setIsOpen(true)
		}else{
			setIsOpen(false)
	}
		
	}

useEffect(()=>{

	if(user.accessToken === null){
		toRegister(false)
	}else{
		toRegister(true)
	}

fetch(`https://arcane-depths-57641.herokuapp.com/products/${id}/product`,{
	method: 'GET',
	header: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
	}
})
.then(res => res.json())
.then(data =>{
		console.log(data);
		setName(data.name);
		setDescription(data.description);
		setPrice(data.price);

})

}, [])


 const addToCart = () => {
//if(typeof(Storage) !== undefined){
	setCart({id, name, description, price, quantity});
	
/*
	

	if(JSON.parse(localStorage.getItem('items')) === null){
		setCart({id, name, description, price, quantity});
		localStorage.setItem('items', JSON.stringify(cart))
	}else{
		const localItems = JSON.parse(localStorage.getItem('items',))

		localItems.map(data=>{
			if(cart.id === data.id){
				cart.quantity + data.quantity
			}else{
				cart.push(data)
			}
		});
		setCart({id, name, description, price, quantity});
	}
	}else{
		alert('local storage is not working.')
	} */
}
 localStorage.setItem('items', JSON.stringify(cart))
return(
	<Row>
		<Col>
			<Card className="cardProduct">
				<Card.Body>
					<div className="bg-dark text-light text-center">
					<Card.Title>
						<h4>{name}</h4>
					</Card.Title>
					</div>
					<Card.Text>
						<h6>Description:</h6> 
						<p>{description}</p>
					</Card.Text>
					<Card.Text>
						<p><strong>Price:</strong> PHP {price}</p> 
					</Card.Text>
					<div className="product-small-input size-sm">
					  <InputGroup className="mb-3">
					    <InputGroup.Prepend>
					    {
					    isOpen ?
					      <Button variant="secondary" onClick={decNum}>&minus;</Button>
					      :
					      <Button variant="secondary" onClick={decNum} disabled>&minus;</Button>
					    }
					    </InputGroup.Prepend>
					    <FormControl value={quantity} onChange={()=>quantity}/>
					     <InputGroup.Prepend>
					      <Button variant="secondary" onClick={incNum}>&#x2B;</Button>
					    </InputGroup.Prepend>
					  </InputGroup>
					 </div>

					 {cartButton ?
					<Button variant="outline-success mr-3" onClick={()=>addToCart()}>Add to Cart </Button>
					:
					<Link to="/login"><Button variant="outline-success mr-3">Login to add to Cart </Button></Link>
					}
						
				</Card.Body>
			</Card>
		</Col>
		</Row>
	

	)
	
}