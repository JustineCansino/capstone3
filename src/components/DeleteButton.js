import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import {useHistory} from 'react-router-dom'


export default function DeleteButton({productId}){


	const history = useHistory

	function deleteProduct(e){
		e.preventDefault();

		fetch(`https://arcane-depths-57641.herokuapp.com/products/${productId}/archive-product`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
				}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Successful',
					icon: 'success',
					text: 'The product is deleted'
				})
				.then(()=>history.go(0))
			}else{
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Failed to delete a product'
				})
			}

		})
	}

	return(
		<Button variant="danger" onClick={deleteProduct}>Delete</Button>
		)


}